package com.realtouchapp.rt_rfid_library;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.acs.bluetooth.Acr1255uj1Reader;
import com.acs.bluetooth.Acr3901us1Reader;
import com.acs.bluetooth.BluetoothReader;
import com.acs.bluetooth.BluetoothReaderGattCallback;
import com.acs.bluetooth.BluetoothReaderManager;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by C.C.Chang on 2017/10/18.
 *
 * @version 1.0.0
 */
@SuppressWarnings("unused")
public final class RTAcsBtCommunicator extends RTRfidCommunicator {
    private final static String TAG = "RTAcsBtCommunicator";

    public interface Listener extends RTRfidCommunicator.Listener {
        void onReaderConnected(RTAcsBtCommunicator communicator, BluetoothReader reader);

        void onCardStatusChanged(RTAcsBtCommunicator communicator, int cardStatus);
        void onResponseToEscapeCommand(RTAcsBtCommunicator communicator, byte[] response, String errorMessage);
        void onResponseToApdu(RTAcsBtCommunicator communicator, byte[] response, String errorMessage);

        void onBatteryStatusChanged(RTAcsBtCommunicator communicator, int status);
        void onBatteryLevelChanged(RTAcsBtCommunicator communicator, int level);
    }

    public static class Builder {
        private Context mContext;
        private String mSpecificDeviceId = null;
        private TransmitPower mTransmitPower = TransmitPower.VeryLow;
        private SleepMode mSleepMode = SleepMode.None;

        public Builder(Context context) {
            super();

            this.mContext = context;
        }

        public Builder setSpecificDeviceId(String specificDeviceId) {
            this.mSpecificDeviceId = specificDeviceId;
            return this;
        }

        public Builder setTransmitPower(TransmitPower transmitPower) {
            this.mTransmitPower = transmitPower;
            return this;
        }

        public Builder setSleepMode(SleepMode sleepMode) {
            this.mSleepMode = sleepMode;
            return this;
        }

        public RTAcsBtCommunicator create() {
            return new RTAcsBtCommunicator(mContext, mSpecificDeviceId, mTransmitPower, mSleepMode);
        }

    }

    private static class DeviceInfo {
        public String systemID;
        public String modelNumber;
        public String serialNumber;
        public String firmwareVersion;
        public String hardwareVersion;
        public String manufacturerName;
    }

    public enum TransmitPower {
        VeryLow,
        Low,
        Medium,
        High
    }

    public enum SleepMode {
        None,
        After60s,
        After90s,
        After120s,
        After180s
    }

    public static boolean isBluetoothOn(Context context) {
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager == null) {
            Log.e(TAG, "Unable to initialize BluetoothManager.");
        }
        else {
            BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
            if (bluetoothAdapter != null) {
                return bluetoothAdapter.isEnabled();
            }
        }

        return false;
    }

    public static void setBluetoothOnOff(Context context, boolean enabled) {
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager == null) {
            Log.e(TAG, "Unable to initialize BluetoothManager.");
        }
        else {
            BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
            if (bluetoothAdapter != null) {
                if (bluetoothAdapter.isEnabled() && !enabled) {
                    bluetoothAdapter.disable();
                }
                else if (!bluetoothAdapter.isEnabled() && enabled) {
                    bluetoothAdapter.enable();
                }
            }
        }

    }

    private Context mContext;
    private String mSpecificDeviceId = null;
    private TransmitPower mTransmitPower = TransmitPower.VeryLow;
    private SleepMode mSleepMode = SleepMode.None;

    private Listener mListener;

    private RTAcsBtCommunicator(Context context,
                                String specificDeviceId,
                                TransmitPower transmitPower,
                                SleepMode sleepMode) {
        super();

        this.mContext = context;
        this.mSpecificDeviceId = specificDeviceId;
        this.mTransmitPower = transmitPower;
        this.mSleepMode = sleepMode;

        BluetoothManager bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager == null) {
            Log.e(TAG, "Unable to initialize BluetoothManager.");
        }
        else {
            mBluetoothAdapter = bluetoothManager.getAdapter();
        }

    }

    @Override
    protected void finalize() throws Throwable {
        disconnectReader();

        super.finalize();
    }

    public void setContext(Context context) {
        this.mContext = context;
    }

    public String getSpecificDeviceId() {
        return mSpecificDeviceId;
    }

    public void setSpecificDeviceId(String specificDeviceId) {

        boolean resetSpecifiDeviceId = false;
        if ((specificDeviceId == null && mSpecificDeviceId != null) ||
                (mSpecificDeviceId == null && specificDeviceId != null)) {

            resetSpecifiDeviceId = true;

        }
        else if (mSpecificDeviceId != null && !mSpecificDeviceId.equals(specificDeviceId)) {

            resetSpecifiDeviceId = true;

        }
        else if (specificDeviceId != null && !specificDeviceId.equals(mSpecificDeviceId)) {

            resetSpecifiDeviceId = true;

        }

        if (resetSpecifiDeviceId) {
            this.mSpecificDeviceId = specificDeviceId;

            if (mBluetoothReader != null && (mSpecificDeviceId == null || mSpecificDeviceId.length() == 0)) {
                // 當已有連線中的裝置，但把mSpecificDeviceId重設成null時，不需要中斷當前連線
                return;
            }

            if (mScanning) {
                scanLeDevice(false);
            }

            disconnectReader();

            if (mListening) {
                scanLeDevice(true);
            }
        }

    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    public TransmitPower getTransmitPower() {
        return mTransmitPower;
    }

    public void setTransmitPower(TransmitPower transmitPower) {
        if (transmitPower != mTransmitPower) {
            this.mTransmitPower = transmitPower;

            if (mBluetoothReader != null) {
                configureTransmitPower();
            }

        }
    }

    public SleepMode getSleepMode() {
        return mSleepMode;
    }

    public void setSleepMode(SleepMode sleepMode) {
        if (sleepMode != mSleepMode) {
            this.mSleepMode = sleepMode;

            if (mBluetoothReader != null) {
                configureSleepMode();
            }

        }
    }

    public boolean isReaderConnected() {
        return (mBluetoothReader != null);
    }

    public String getReaderName() {
        if (mBluetoothDevice == null) {
            return null;
        }

        return mBluetoothDevice.getName();
    }

    public String getSystemID() {
        if (mDeviceInfo == null) {
            return null;
        }

        return mDeviceInfo.systemID;
    }

    public String getModelNumber() {
        if (mDeviceInfo == null) {
            return null;
        }

        return mDeviceInfo.modelNumber;
    }

    public String getSerialNumber() {
        if (mDeviceInfo == null) {
            return null;
        }

        return mDeviceInfo.serialNumber;
    }

    public String getFirmwareVersion() {
        if (mDeviceInfo == null) {
            return null;
        }

        return mDeviceInfo.firmwareVersion;
    }

    public String getHardwareVersion() {
        if (mDeviceInfo == null) {
            return null;
        }

        return mDeviceInfo.hardwareVersion;
    }

    public String getManufacturerName() {
        if (mDeviceInfo == null) {
            return null;
        }

        return mDeviceInfo.manufacturerName;
    }

    // -- RTRfidCommunicator

    private boolean mListening = false;

    private boolean mScanning = false;

    /* Stops scanning after 30 seconds. */
    private static final long SCAN_PERIOD = 30000;

    private BluetoothAdapter mBluetoothAdapter;

    @Override
    public void start() {
        if (mBluetoothAdapter == null) {
            BluetoothManager bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
            if (bluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                postErrorWithoutListening("Unable to initialize BluetoothManager.");
                return;
            }

            mBluetoothAdapter = bluetoothManager.getAdapter();
            if (mBluetoothAdapter == null) {
                Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
                postErrorWithoutListening("Unable to obtain a BluetoothAdapter.");
                return;
            }
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Log.e(TAG, "Bluetooth not open.");
            postErrorWithoutListening("Bluetooth not open.");
            return;
        }

        mListening = true;

        if (mBluetoothReader == null) {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);

            scanLeDevice(true);
        }

    }

    @Override
    public void stop() {
        mListening = false;

        scanLeDevice(false);

    }

    @Override
    public void sendApdu(byte[] apdu) {
        if (mBluetoothReader == null) {
            return;
        }
        if (apdu == null || apdu.length == 0) {
            return;
        }

        try {
            mBluetoothReader.transmitApdu(apdu);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void powerOffCard() {
        if (mBluetoothReader == null) {
            return;
        }

        mBluetoothReader.powerOffCard();
    }

    // --

    public void sendEscapeCommand(byte[] escapeCommand) {
        if (mBluetoothReader == null) {
            return;
        }
        if (escapeCommand == null || escapeCommand.length == 0) {
            return;
        }

        try {
            mBluetoothReader.transmitEscapeCommand(escapeCommand);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void forceDisconnect() {
        stop();

        disconnectReader();

    }

    private void postError(final String errorMessage) {
        if (!mListening) {
            return;
        }

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.onError(RTAcsBtCommunicator.this, errorMessage);
                }
            }
        });


    }

    private void postErrorWithoutListening(final String errorMessage) {

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.onError(RTAcsBtCommunicator.this, errorMessage);
                }
            }
        });

    }

    private synchronized void scanLeDevice(final boolean enable) {
        if (enable) {
            /* Stops scanning after a pre-defined scan period. */
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mScanning) {
                        mScanning = false;
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);

                        if (mListening) {
                            // 5秒後重啟搜尋
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    scanLeDevice(true);
                                }
                            }, 5000);
                        }
                    }
                }
            }, SCAN_PERIOD);

            mScanDevices.clear();
            mDetectingDevices.clear();

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        }
        else if (mScanning) {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private ArrayList<BluetoothDevice> mScanDevices = new ArrayList<>();

    /* Device scan callback. */
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device,
                             int rssi,
                             byte[] scanRecord) {

            if (!mScanDevices.contains(device)) {
                mScanDevices.add(device);

                Log.e(TAG, "didScanDevice: "+(device.getName()==null?"Unknown":device.getName()));

                if (mSpecificDeviceId != null && mSpecificDeviceId.length() > 0) {
                    String specificDeviceId = mSpecificDeviceId;
                    if (specificDeviceId.contains("-")) {
                        String[] splitString = specificDeviceId.split("-");
                        specificDeviceId = splitString[splitString.length-1];
                    }
                    String name = device.getName();
                    if (name != null && name.contains("-")) {
                        String[] splitString = name.split("-");
                        name = splitString[splitString.length-1];
                    }

                    if (specificDeviceId.equals(name)) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                connectReader(device);
                            }
                        });
                    }
                }
                else {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            connectReader(device);
                        }
                    });
                }

            }

        }
    };

    private enum CommandType {
        None,
        TransmitPower,
        SleepMode,
        StartPolling,
        StopPolling,
        GetTagID,
        Others
    }

    private CommandType mCmdType;
    private String mAtrString;

    private BluetoothDevice mBluetoothDevice;
    private BluetoothGatt mBluetoothGatt;
    /* Detected reader. */
    private BluetoothReader mBluetoothReader;
    private BluetoothReaderGattCallback mGattCallback;

    private DeviceInfo mDeviceInfo;

    private ArrayList<ReaderDetector> mDetectingDevices = new ArrayList<>();

    /*
     * Create a GATT connection with the reader. And detect the connected reader
     * once service list is available.
     */
    private void connectReader(BluetoothDevice device) {
        if (device == null) {
            return;
        }

        for (ReaderDetector detector : mDetectingDevices) {
            if (detector.mBluetoothDevice.getAddress().equals(device.getAddress())) {
                return;
            }
        }

        ReaderDetector.Callback callback = new ReaderDetector.Callback() {
            @Override
            public void onReaderDetected(ReaderDetector detector, BluetoothReader reader) {
                if (reader == null) {
                    if (detector.mBluetoothDevice != null && mScanDevices.contains(detector.mBluetoothDevice)) {
                        mScanDevices.remove(detector.mBluetoothDevice);
                    }

                    mDetectingDevices.remove(detector);

                    return;
                }

                if (mBluetoothReader == null) {
                    mBluetoothReader = reader;

                    scanLeDevice(false);

                    mCmdType = CommandType.None;
                    mAtrString = null;

                    mDeviceInfo = null;

                    mBluetoothDevice = detector.mBluetoothDevice;
                    mBluetoothGatt = detector.mBluetoothGatt;
                    mGattCallback = detector.mGattCallback;
                    configureGattCallback();

                    setListener();
                    activateReader(reader);
                }

                mDetectingDevices.clear();

            }

            @Override
            public void onFailed(ReaderDetector detector) {
                if (detector.mBluetoothDevice != null && mScanDevices.contains(detector.mBluetoothDevice)) {
                    mScanDevices.remove(detector.mBluetoothDevice);
                }

                mDetectingDevices.remove(detector);

            }
        };

        ReaderDetector detector = new ReaderDetector(mContext, device, callback);
        mDetectingDevices.add(detector);

        detector.connectReader();

    }

    /* Disconnects an established connection. */
    private void disconnectReader() {
        if (mBluetoothGatt == null) {

            mBluetoothReader = null;

            if (mBluetoothDevice != null && mScanDevices.contains(mBluetoothDevice)) {
                mScanDevices.remove(mBluetoothDevice);
            }
            mBluetoothDevice = null;

            mGattCallback = null;

            mCmdType = CommandType.None;
            mAtrString = null;

            mDeviceInfo = null;

            return;
        }
        mBluetoothGatt.disconnect();

        mBluetoothReader = null;

        if (mBluetoothDevice != null && mScanDevices.contains(mBluetoothDevice)) {
            mScanDevices.remove(mBluetoothDevice);
        }
        mBluetoothDevice = null;

        mDeviceInfo = null;

    }

    private void configureGattCallback() {
        mGattCallback.setOnConnectionStateChangeListener(new BluetoothReaderGattCallback.OnConnectionStateChangeListener() {

            @Override
            public void onConnectionStateChange(
                    final BluetoothGatt gatt, final int state,
                    final int newState) {

                Log.e(TAG, "onConnectionStateChange: "+state+", newState: "+newState);
                if (state != BluetoothGatt.GATT_SUCCESS) {
                    boolean shouldPostDisconnected = false;
                    if (mBluetoothReader != null) {
                        shouldPostDisconnected = true;
                    }
                    mBluetoothReader = null;
                    if (mBluetoothDevice != null && mScanDevices.contains(mBluetoothDevice)) {
                        mScanDevices.remove(mBluetoothDevice);
                    }
                    mBluetoothDevice = null;
                    /*
                     * Release resources occupied by Bluetooth
                     * GATT client.
                     */
                    if (mBluetoothGatt != null) {
                        mBluetoothGatt.close();
                    }
                    mBluetoothGatt = null;

                    mGattCallback = null;

                    mCmdType = CommandType.None;
                    mAtrString = null;

                    mDeviceInfo = null;

                    if (shouldPostDisconnected) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mListening && mListener != null) {
                                    mListener.onReaderDisconnected(RTAcsBtCommunicator.this);
                                }
                            }
                        });
                    }

                    return;
                }

                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    Log.e(TAG, "on GATT connected");
                }
                else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    mBluetoothReader = null;
                    if (mBluetoothDevice != null && mScanDevices.contains(mBluetoothDevice)) {
                        mScanDevices.remove(mBluetoothDevice);
                    }
                    mBluetoothDevice = null;
                    /*
                     * Release resources occupied by Bluetooth
                     * GATT client.
                     */
                    if (mBluetoothGatt != null) {
                        mBluetoothGatt.close();
                    }
                    mBluetoothGatt = null;

                    mGattCallback = null;

                    mCmdType = CommandType.None;
                    mAtrString = null;

                    mDeviceInfo = null;

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mListening && mListener != null) {
                                mListener.onReaderDisconnected(RTAcsBtCommunicator.this);
                            }
                        }
                    });

                }
            }
        });
    }

    /*
     * Update listener
     */
    private void setListener() {
        if (mBluetoothReader == null) {
            return;
        }

        /* Update status change listener */
        if (mBluetoothReader instanceof Acr3901us1Reader) {
            ((Acr3901us1Reader) mBluetoothReader)
                    .setOnBatteryStatusChangeListener(new Acr3901us1Reader.OnBatteryStatusChangeListener() {

                        @Override
                        public void onBatteryStatusChange(
                                BluetoothReader bluetoothReader,
                                final int batteryStatus) {

                            Log.e(TAG, "mBatteryStatusListener data: " + batteryStatus);
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (mListening && mListener != null) {
                                        mListener.onBatteryStatusChanged(RTAcsBtCommunicator.this, batteryStatus);
                                    }
                                }
                            });

                        }

                    });
        } else if (mBluetoothReader instanceof Acr1255uj1Reader) {
            ((Acr1255uj1Reader) mBluetoothReader)
                    .setOnBatteryLevelChangeListener(new Acr1255uj1Reader.OnBatteryLevelChangeListener() {

                        @Override
                        public void onBatteryLevelChange(
                                BluetoothReader bluetoothReader,
                                final int batteryLevel) {

                            Log.e(TAG, "mBatteryLevelListener data: " + batteryLevel);
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (mListening && mListener != null) {
                                        mListener.onBatteryLevelChanged(RTAcsBtCommunicator.this, batteryLevel);
                                    }
                                }
                            });

                        }

                    });
        }
        mBluetoothReader
                .setOnCardStatusChangeListener(new BluetoothReader.OnCardStatusChangeListener() {

                    @Override
                    public void onCardStatusChange(
                            BluetoothReader bluetoothReader, final int sta) {

                        Log.e(TAG, "mCardStatusListener sta: " + sta);
                        switch (sta) {
                            case BluetoothReader.CARD_STATUS_PRESENT: {

                                mAtrString = null;

                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mBluetoothReader != null && !mBluetoothReader.powerOnCard()) {
                                            postError("Card reader is not ready");
                                        }
                                    }
                                }, 50);

                                break;
                            }
                            case BluetoothReader.CARD_STATUS_POWERED: {
                                break;
                            }
                            default:
                                break;
                        }

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mListening && mListener != null) {
                                    mListener.onCardStatusChanged(RTAcsBtCommunicator.this, sta);
                                }
                            }
                        });


                    }

                });

        /* Wait for authentication completed. */
        mBluetoothReader
                .setOnAuthenticationCompleteListener(new BluetoothReader.OnAuthenticationCompleteListener() {

                    @Override
                    public void onAuthenticationComplete(
                            BluetoothReader bluetoothReader, final int errorCode) {

                        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
                            Log.e(TAG, "Authentication Success!");

                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    startPollingProcess();
                                }
                            });

                        }
                        else {
                            Log.e(TAG, "Authentication Failed!");
                            disconnectReader();
                        }

                    }

                });

        /* Wait for receiving ATR string. */
        mBluetoothReader
                .setOnAtrAvailableListener(new BluetoothReader.OnAtrAvailableListener() {

                    @Override
                    public void onAtrAvailable(BluetoothReader bluetoothReader,
                                               final byte[] atr, final int errorCode) {

                        if (atr == null) {
                            Log.e(TAG, "onAtrAvailable: "+getErrorString(errorCode));

                            postError(getErrorString(errorCode));

                        }
                        else {
                            Log.e(TAG, "onAtrAvailable: "+ADBHex.toHexString(atr));
                            mAtrString = ADBHex.toHexString(atr);

                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    mCmdType = CommandType.GetTagID;
                                    getCardSerialNumber();
                                }
                            });

                        }

                    }

                });

        /* Wait for power off response. */
        mBluetoothReader
                .setOnCardPowerOffCompleteListener(new BluetoothReader.OnCardPowerOffCompleteListener() {

                    @Override
                    public void onCardPowerOffComplete(
                            BluetoothReader bluetoothReader, final int result) {

                        Log.e(TAG, "onCardPowerOffComplete: "+getErrorString(result));

                    }

                });

        /* Wait for response APDU. */
        mBluetoothReader
                .setOnResponseApduAvailableListener(new BluetoothReader.OnResponseApduAvailableListener() {

                    @Override
                    public void onResponseApduAvailable(
                            BluetoothReader bluetoothReader, final byte[] apdu,
                            final int errorCode) {

                        Log.e(TAG, "onResponseApduAvailable: "+getResponseString(apdu, errorCode));

                        String errorMessage = null;
                        if (errorCode != BluetoothReader.ERROR_SUCCESS) {
                            errorMessage = getErrorString(errorCode);
                        }

                        final String errMsg = errorMessage;
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mListening && mListener != null) {
                                    mListener.onResponseToApdu(RTAcsBtCommunicator.this, apdu, errMsg);
                                }
                            }
                        });

                    }

                });

        /* Wait for escape command response. */
        mBluetoothReader
                .setOnEscapeResponseAvailableListener(new BluetoothReader.OnEscapeResponseAvailableListener() {

                    @Override
                    public void onEscapeResponseAvailable(
                            BluetoothReader bluetoothReader,
                            final byte[] response, final int errorCode) {

                        Log.e(TAG, "onEscapeResponseAvailable: "+getResponseString(response, errorCode));

                        String errorMessage = null;
                        if (errorCode != BluetoothReader.ERROR_SUCCESS) {
                            errorMessage = getErrorString(errorCode);
                        }

                        CommandType cmdType = mCmdType;
                        mCmdType = CommandType.None;

                        switch (cmdType) {
                            case StartPolling: {

                                if (errorMessage == null) {
                                    mHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (mListening && mListener != null) {
                                                mListener.onReaderConnected(RTAcsBtCommunicator.this, mBluetoothReader);
                                            }
                                        }
                                    });

                                }
                                else {
                                    postError(errorMessage);
                                }

                                break;
                            }
                            case GetTagID: {

                                if (response == null) {
                                    break;
                                }
                                if (response.length < 2) {
                                    break;
                                }

                                String tagID = ADBHex.toHexString(response);
                                tagID = tagID.replace(" ", "");

                                byte checkByte1 = response[response.length-2];
                                byte checkByte2 = response[response.length-1];
                                if (checkByte1 == (byte) 0x90 && checkByte2 == 0x00) {
                                    tagID = tagID.toUpperCase(Locale.US);
                                    tagID = tagID.substring(0, tagID.length()-4);
                                }
                                else if (checkByte1 == (byte) 0x62 && checkByte2 == (byte) 0x82) {
                                    errorMessage = "End of UID/ATS reached before Le bytes (Le is greater than UID Length)";
                                }
                                else if (checkByte1 == (byte) 0x63 && checkByte2 == 0x00) {
                                    errorMessage = "The operation is failed";
                                }
                                else if (checkByte1 == (byte) 0x6A && checkByte2 == (byte) 0x81) {
                                    errorMessage = "Function not supported";
                                }
                                else if (checkByte1 == (byte) 0x6C) {
                                    errorMessage = "Wrong length (wrong number Le: ‘"+checkByte2+"’ encodes the exact number) if Le is less than the available UID length";
                                }
                                else {
                                    errorMessage = "Other failure";
                                }

                                final String finalTagID = tagID;
                                final String finalErrMsg = errorMessage;
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mListening && mListener != null) {
                                            mListener.onFindTag(RTAcsBtCommunicator.this, finalTagID, mAtrString, finalErrMsg);
                                        }
                                    }
                                });

                                break;
                            }
//                            case None:
//                            case TransmitPower:
//                            case SleepMode:
//                            case StopPolling:
//                                break;
                            default: {

                                final String finalErrMsg = errorMessage;
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mListening && mListener != null) {
                                            mListener.onResponseToEscapeCommand(RTAcsBtCommunicator.this, response, finalErrMsg);
                                        }
                                    }
                                });

                                break;
                            }
                        }

                    }

                });

        /* Wait for device info available. */
        mBluetoothReader
                .setOnDeviceInfoAvailableListener(new BluetoothReader.OnDeviceInfoAvailableListener() {

                    @Override
                    public void onDeviceInfoAvailable(
                            BluetoothReader bluetoothReader, final int infoId,
                            final Object o, final int status) {

                        if (status != BluetoothGatt.GATT_SUCCESS) {
                            Log.e(TAG, "Failed to read device info!");
                            return;
                        }

                        if (mDeviceInfo == null) {
                            mDeviceInfo = new DeviceInfo();
                        }

                        switch (infoId) {
                            case BluetoothReader.DEVICE_INFO_SYSTEM_ID: {
                                Log.e(TAG, "System ID: "+ADBHex.toHexString((byte[]) o));
                                mDeviceInfo.systemID = ADBHex.toHexString((byte[]) o);
                                break;
                            }
                            case BluetoothReader.DEVICE_INFO_MODEL_NUMBER_STRING: {
                                Log.e(TAG, "Model Number: "+o);
                                mDeviceInfo.modelNumber = (String) o;
                                break;
                            }
                            case BluetoothReader.DEVICE_INFO_SERIAL_NUMBER_STRING: {
                                Log.e(TAG, "Serial Number: "+o);
                                mDeviceInfo.serialNumber = (String) o;

                                String serialNumber = (String) o;
                                if (mSpecificDeviceId != null && mSpecificDeviceId.length() > 0) {
                                    if (!serialNumber.equalsIgnoreCase(mSpecificDeviceId) &&
                                            !serialNumber.contains(mSpecificDeviceId)) {

                                        disconnectReader();

                                    }
                                    else {
                                        mHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                authenticateReader();
                                            }
                                        });
                                    }
                                }

                                break;
                            }
                            case BluetoothReader.DEVICE_INFO_FIRMWARE_REVISION_STRING: {
                                Log.e(TAG, "Firmware Revision: "+o);
                                mDeviceInfo.firmwareVersion = (String) o;
                                break;
                            }
                            case BluetoothReader.DEVICE_INFO_HARDWARE_REVISION_STRING: {
                                Log.e(TAG, "Hardware Revision: "+o);
                                mDeviceInfo.hardwareVersion = (String) o;
                                break;
                            }
                            case BluetoothReader.DEVICE_INFO_MANUFACTURER_NAME_STRING: {
                                Log.e(TAG, "Manufacturer Name: "+o);
                                mDeviceInfo.manufacturerName = (String) o;
                                break;
                            }
                            default:
                                break;
                        }

                    }

                });

        /* Wait for battery level available. */
        if (mBluetoothReader instanceof Acr1255uj1Reader) {
            ((Acr1255uj1Reader) mBluetoothReader)
                    .setOnBatteryLevelAvailableListener(new Acr1255uj1Reader.OnBatteryLevelAvailableListener() {

                        @Override
                        public void onBatteryLevelAvailable(
                                BluetoothReader bluetoothReader,
                                final int batteryLevel, int status) {
                            Log.e(TAG, "mBatteryLevelListener data: " + batteryLevel);

                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (mListening && mListener != null) {
                                        mListener.onBatteryLevelChanged(RTAcsBtCommunicator.this, batteryLevel);
                                    }
                                }
                            });


                        }

                    });
        }

        /* Handle on battery status available. */
        if (mBluetoothReader instanceof Acr3901us1Reader) {
            ((Acr3901us1Reader) mBluetoothReader)
                    .setOnBatteryStatusAvailableListener(new Acr3901us1Reader.OnBatteryStatusAvailableListener() {

                        @Override
                        public void onBatteryStatusAvailable(
                                BluetoothReader bluetoothReader,
                                final int batteryStatus, int status) {

                            Log.e(TAG, "onBatteryStatusAvailable: " + getBatteryStatusString(batteryStatus));

                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (mListening && mListener != null) {
                                        mListener.onBatteryStatusChanged(RTAcsBtCommunicator.this, batteryStatus);
                                    }
                                }
                            });

                        }

                    });
        }

        /* Handle on slot status available. */
        mBluetoothReader
                .setOnCardStatusAvailableListener(new BluetoothReader.OnCardStatusAvailableListener() {

                    @Override
                    public void onCardStatusAvailable(
                            BluetoothReader bluetoothReader,
                            final int cardStatus, final int errorCode) {

                        if (errorCode != BluetoothReader.ERROR_SUCCESS) {
                            Log.e(TAG, "onCardStatusAvailable: " + getErrorString(errorCode));

                            postError(getErrorString(errorCode));

                        }
                        else {
                            Log.e(TAG, "onCardStatusAvailable: " + getCardStatusString(cardStatus));

                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (mListening && mListener != null) {
                                        mListener.onCardStatusChanged(RTAcsBtCommunicator.this, cardStatus);
                                    }
                                }
                            });

                        }

                    }

                });

        mBluetoothReader
                .setOnEnableNotificationCompleteListener(new BluetoothReader.OnEnableNotificationCompleteListener() {

                    @Override
                    public void onEnableNotificationComplete(
                            BluetoothReader bluetoothReader, final int result) {

                        if (result != BluetoothGatt.GATT_SUCCESS) {
                            Log.e(TAG, "The device is unable to set notification!");

                            disconnectReader();
                        }
                        else {
                            Log.e(TAG, "The device is ready to use!");

                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getDeviceInfo();

                                    if (mSpecificDeviceId == null || mSpecificDeviceId.length() == 0) {
                                        authenticateReader();
                                    }

                                }
                            });

                        }

                    }

                });
    }

    /* Start the process to enable the reader's notifications. */
    private void activateReader(BluetoothReader reader) {
        if (reader == null) {
            return;
        }

        if (reader instanceof Acr3901us1Reader) {
            /* Start pairing to the reader. */
            ((Acr3901us1Reader) reader).startBonding();
        }
        else if (reader instanceof Acr1255uj1Reader) {
            /* Enable notification. */
            reader.enableNotification(true);
        }
    }

    private void getDeviceInfo() {
        if (mBluetoothReader == null) {
            return;
        }
        if (!(mBluetoothReader instanceof Acr3901us1Reader)
                && !(mBluetoothReader instanceof Acr1255uj1Reader)) {
            disconnectReader();
            return;
        }

        if (mBluetoothReader
                .getDeviceInfo(BluetoothReader.DEVICE_INFO_MANUFACTURER_NAME_STRING)) {
            mBluetoothReader
                    .getDeviceInfo(BluetoothReader.DEVICE_INFO_SYSTEM_ID);
            mBluetoothReader
                    .getDeviceInfo(BluetoothReader.DEVICE_INFO_MODEL_NUMBER_STRING);
            mBluetoothReader
                    .getDeviceInfo(BluetoothReader.DEVICE_INFO_SERIAL_NUMBER_STRING);
            mBluetoothReader
                    .getDeviceInfo(BluetoothReader.DEVICE_INFO_FIRMWARE_REVISION_STRING);
            mBluetoothReader
                    .getDeviceInfo(BluetoothReader.DEVICE_INFO_HARDWARE_REVISION_STRING);
        }
        else {
            disconnectReader();
        }

    }

    /* Default master key. */
    private static final String DEFAULT_3901_MASTER_KEY = "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF";
    /* Default master key. */
    private static final String DEFAULT_1255_MASTER_KEY = "ACR1255U-J1 Auth";
    private void authenticateReader() {
        if (mBluetoothReader == null) {
            return;
        }

        byte[] masterKey;
        if (mBluetoothReader instanceof Acr3901us1Reader) {
            masterKey = ADBHex.hexString2Bytes(DEFAULT_3901_MASTER_KEY.replace(" ", ""));
        }
        else if (mBluetoothReader instanceof Acr1255uj1Reader) {
            try {
                masterKey = DEFAULT_1255_MASTER_KEY.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

                return;
            }
        }
        else {
            return;
        }

        if (!mBluetoothReader.authenticate(masterKey)) {
            disconnectReader();
        }

    }

    private void startPollingProcess() {
        mCmdType = CommandType.TransmitPower;
        configureTransmitPower();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCmdType = CommandType.SleepMode;
                configureSleepMode();
            }
        }, 300);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCmdType = CommandType.StartPolling;
                startPolling();
            }
        }, 600);
    }

    private void configureTransmitPower() {
        if (mBluetoothReader == null) {
            return;
        }
        if (!(mBluetoothReader instanceof Acr1255uj1Reader)) {
            return;
        }

        byte[] command = { (byte) 0xE0, 0x00, 0x00, 0x49, 0x00 };
        switch (mTransmitPower) {
            case Low:
                command[4] = 0x01;
                break;
            case Medium:
                command[4] = 0x02;
                break;
            case High:
                command[4] = 0x03;
                break;
            default:
                break;
        }

        Log.e(TAG, "send escape command: "+ADBHex.toHexString(command));
        if (!mBluetoothReader.transmitEscapeCommand(command)) {
            postError("Card reader is not ready");
        }

    }

    private void configureSleepMode() {
        if (mBluetoothReader == null) {
            return;
        }
        if (!(mBluetoothReader instanceof Acr1255uj1Reader)) {
            return;
        }

        byte[] command = { (byte) 0xE0, 0x00, 0x00, 0x48, 0x04 };
        switch (mSleepMode) {
            case After60s:
                command[4] = 0x00;
                break;
            case After90s:
                command[4] = 0x01;
                break;
            case After120s:
                command[4] = 0x02;
                break;
            case After180s:
                command[4] = 0x03;
                break;
            default:
                break;
        }

        Log.e(TAG, "send escape command: "+ADBHex.toHexString(command));
        if (!mBluetoothReader.transmitEscapeCommand(command)) {
            postError("Card reader is not ready");
        }

    }

    private static final byte[] AUTO_POLLING_START = { (byte) 0xE0, 0x00, 0x00,
            0x40, 0x01 };
    private void startPolling() {
        if (mBluetoothReader == null) {
            return;
        }
        if (!mBluetoothReader.transmitEscapeCommand(AUTO_POLLING_START)) {
            postError("Card reader is not ready");
        }
    }

    private static final byte[] AUTO_POLLING_STOP = { (byte) 0xE0, 0x00, 0x00,
            0x40, 0x00 };
    private void stopPolling() {
        if (mBluetoothReader == null) {
            return;
        }
        if (!mBluetoothReader.transmitEscapeCommand(AUTO_POLLING_STOP)) {
            postError("Card reader is not ready");
        }
    }

    private void getCardSerialNumber() {
        if (mBluetoothReader == null) {
            return;
        }
        if (!(mBluetoothReader instanceof Acr1255uj1Reader)) {
            return;
        }

        byte[] command = { (byte) 0xFF, (byte) 0xCA, 0x00, 0x00, 0x00 };

        Log.e(TAG, "send escape command: "+ADBHex.toHexString(command));
        if (!mBluetoothReader.transmitEscapeCommand(command)) {
            postError("Card reader is not ready");
        }

    }

    /* Get the Battery level string. */
    private String getBatteryLevelString(int batteryLevel) {
        if (batteryLevel < 0 || batteryLevel > 100) {
            return "Unknown.";
        }

        return String.valueOf(batteryLevel) + "%";
    }

    /* Get the Battery status string. */
    private String getBatteryStatusString(int batteryStatus) {
        if (batteryStatus == BluetoothReader.BATTERY_STATUS_NONE) {
            return "No battery.";
        }
        else if (batteryStatus == BluetoothReader.BATTERY_STATUS_FULL) {
            return "The battery is full.";
        }
        else if (batteryStatus == BluetoothReader.BATTERY_STATUS_USB_PLUGGED) {
            return "The USB is plugged.";
        }

        return "The battery is low.";
    }

    /* Get the Bonding status string. */
    private String getBondingStatusString(int bondingStatus) {
        if (bondingStatus == BluetoothDevice.BOND_BONDED) {
            return "BOND BONDED";
        }
        else if (bondingStatus == BluetoothDevice.BOND_NONE) {
            return "BOND NONE";
        }
        else if (bondingStatus == BluetoothDevice.BOND_BONDING) {
            return "BOND BONDING";
        }

        return "BOND UNKNOWN.";
    }

    /* Get the Card status string. */
    private String getCardStatusString(int cardStatus) {
        if (cardStatus == BluetoothReader.CARD_STATUS_ABSENT) {
            return "Absent.";
        }
        else if (cardStatus == BluetoothReader.CARD_STATUS_PRESENT) {
            return "Present.";
        }
        else if (cardStatus == BluetoothReader.CARD_STATUS_POWERED) {
            return "Powered.";
        }
        else if (cardStatus == BluetoothReader.CARD_STATUS_POWER_SAVING_MODE) {
            return "Power saving mode.";
        }

        return "The card status is unknown.";
    }

    /* Get the Error string. */
    private String getErrorString(int errorCode) {
        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
            return "";
        }
        else if (errorCode == BluetoothReader.ERROR_INVALID_CHECKSUM) {
            return "The checksum is invalid.";
        }
        else if (errorCode == BluetoothReader.ERROR_INVALID_DATA_LENGTH) {
            return "The data length is invalid.";
        }
        else if (errorCode == BluetoothReader.ERROR_INVALID_COMMAND) {
            return "The command is invalid.";
        }
        else if (errorCode == BluetoothReader.ERROR_UNKNOWN_COMMAND_ID) {
            return "The command ID is unknown.";
        }
        else if (errorCode == BluetoothReader.ERROR_CARD_OPERATION) {
            return "The card operation failed.";
        }
        else if (errorCode == BluetoothReader.ERROR_AUTHENTICATION_REQUIRED) {
            return "Authentication is required.";
        }
        else if (errorCode == BluetoothReader.ERROR_LOW_BATTERY) {
            return "The battery is low.";
        }
        else if (errorCode == BluetoothReader.ERROR_CHARACTERISTIC_NOT_FOUND) {
            return "Error characteristic is not found.";
        }
        else if (errorCode == BluetoothReader.ERROR_WRITE_DATA) {
            return "Write command to reader is failed.";
        }
        else if (errorCode == BluetoothReader.ERROR_TIMEOUT) {
            return "Timeout.";
        }
        else if (errorCode == BluetoothReader.ERROR_AUTHENTICATION_FAILED) {
            return "Authentication is failed.";
        }
        else if (errorCode == BluetoothReader.ERROR_UNDEFINED) {
            return "Undefined error.";
        }
        else if (errorCode == BluetoothReader.ERROR_INVALID_DATA) {
            return "Received data error.";
        }
        else if (errorCode == BluetoothReader.ERROR_COMMAND_FAILED) {
            return "The command failed.";
        }

        return "Unknown error.";
    }

    /* Get the Response string. */
    private String getResponseString(byte[] response, int errorCode) {
        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
            if (response != null && response.length > 0) {
                return ADBHex.toHexString(response);
            }
            return "";
        }
        return getErrorString(errorCode);
    }

    private static class ReaderDetector {

        public interface Callback {
            void onReaderDetected(ReaderDetector detector, BluetoothReader reader);
            void onFailed(ReaderDetector detector);
        }

        public BluetoothDevice mBluetoothDevice;
        public BluetoothGatt mBluetoothGatt;
        /* ACS Bluetooth reader library. */
        private BluetoothReaderManager mBluetoothReaderManager;
        public BluetoothReaderGattCallback mGattCallback;

        private Context mContext;
        private Handler mHandler;
        private Callback mCallback;

        public ReaderDetector(Context context, BluetoothDevice device, Callback callback) {
            super();

            this.mContext = context;
            this.mCallback = callback;
            this.mHandler = new Handler(Looper.getMainLooper());
            this.mBluetoothDevice = device;

            /* Initialize mBluetoothReaderManager. */
            mBluetoothReaderManager = new BluetoothReaderManager();
            /* Register BluetoothReaderManager's listeners */
            mBluetoothReaderManager.setOnReaderDetectionListener(new BluetoothReaderManager.OnReaderDetectionListener() {

                @Override
                public void onReaderDetection(final BluetoothReader reader) {
                    if (reader instanceof Acr3901us1Reader) {
                            /* The connected reader is ACR3901U-S1 reader. */
                        Log.e(TAG, "On Acr3901us1Reader Detected.");
                    }
                    else if (reader instanceof Acr1255uj1Reader) {
                            /* The connected reader is ACR1255U-J1 reader. */
                        Log.e(TAG, "On Acr1255uj1Reader Detected.");
                    }
                    else {
                            /* Disconnect Bluetooth reader */
                        Log.e(TAG, "Disconnect reader!!!");
                        disconnectReader();
                        return;
                    }

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mCallback != null) {
                                mCallback.onReaderDetected(ReaderDetector.this, reader);
                            }
                        }
                    });


                }
            });

        }

        @Override
        protected void finalize() throws Throwable {
            disconnectReader();

            super.finalize();
        }

        private void postFailed() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mCallback != null) {
                        mCallback.onFailed(ReaderDetector.this);
                    }
                }
            });
        }

        /*
         * Create a GATT connection with the reader. And detect the connected reader
         * once service list is available.
         */
        public void connectReader() {
            if (mBluetoothDevice == null) {
                postFailed();
                return;
            }

            configureGattCallback();

            /*
             * Connect Device.
             */
            /* Connect to GATT server. */
            mBluetoothGatt = mBluetoothDevice.connectGatt(mContext, false, mGattCallback);

        }

        /* Disconnects an established connection. */
        private void disconnectReader() {
            if (mBluetoothGatt == null) {
                postFailed();
                return;
            }
            mBluetoothGatt.disconnect();

            mBluetoothDevice = null;

        }

        private void configureGattCallback() {
            mGattCallback = new BluetoothReaderGattCallback();

            mGattCallback.setOnConnectionStateChangeListener(new BluetoothReaderGattCallback.OnConnectionStateChangeListener() {

                @Override
                public void onConnectionStateChange(
                        final BluetoothGatt gatt, final int state,
                        final int newState) {

                    Log.e(TAG, "onConnectionStateChange: "+state+", newState: "+newState);

                    if (state != BluetoothGatt.GATT_SUCCESS) {
                        mBluetoothDevice = null;
                        /*
                         * Release resources occupied by Bluetooth
                         * GATT client.
                         */
                        if (mBluetoothGatt != null) {
                            mBluetoothGatt.close();
                        }
                        mBluetoothGatt = null;

                        mGattCallback = null;

                        postFailed();

                        return;
                    }

                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        /* Detect the connected reader. */
                        if (mBluetoothReaderManager != null) {
                            mBluetoothReaderManager.detectReader(gatt, mGattCallback);
                        }
                        else {
                            postFailed();
                        }

                    }
                    else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        mBluetoothDevice = null;
                        /*
                         * Release resources occupied by Bluetooth
                         * GATT client.
                         */
                        if (mBluetoothGatt != null) {
                            mBluetoothGatt.close();
                        }
                        mBluetoothGatt = null;

                        mGattCallback = null;

                        postFailed();

                    }
                }
            });
        }

    }

}
