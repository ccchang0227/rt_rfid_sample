package com.realtouchapp.rt_rfid_library;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by C.C.Chang on 2017/10/18.
 *
 * @version 1
 */

public abstract class RTRfidCommunicator {

    public interface Listener {
        /**錯誤*/
        void onError(RTRfidCommunicator communicator, String errorMessage);
        /**Reader斷線*/
        void onReaderDisconnected(RTRfidCommunicator communicator);
        /**找到 TAG*/
        void onFindTag(RTRfidCommunicator communicator, String tagID, String atrString, String errorMessage);
    }

    protected Handler mHandler;

    public RTRfidCommunicator() {
        super();

        this.mHandler = new Handler(Looper.getMainLooper());

    }

    public abstract void start();
    public abstract void stop();

    public abstract void sendApdu(byte[] apdu);

    public abstract void powerOffCard();

}
