package com.realtouchapp.rt_rfid_library;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.util.Log;

import com.acs.smartcard.Reader;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by C.C.Chang on 2017/12/7.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public final class RTAcsUsbCommunicator extends RTRfidCommunicator {
    private static final String TAG = "RTAcsUsbCommunicator";

    private static final String ACTION_USB_PERMISSION = "com.realtouchapp.rt_rfid_library.USB_PERMISSION";

    public interface Listener extends RTRfidCommunicator.Listener {
        /**Reader連線*/
        void onReaderConnected(RTAcsUsbCommunicator communicator, Reader reader);
        /**TAG狀態變化 */
        void onCardStatusChanged(RTAcsUsbCommunicator communicator, int cardStatus);
        /**送出訊息至TAG 的回傳 */
        void onResponseToApdu(RTAcsUsbCommunicator communicator, byte[] response, String errorMessage);
        /**讀取 TAG 資料 */
        void onReadData(RTAcsUsbCommunicator communicator, byte[] response, String errorMessage);
        /**執行指令 */
        void onTransmit(RTAcsUsbCommunicator communicator, String commandString);
    }

    public static List<UsbDevice> getSupportedDevices(Context context) {

        ArrayList<UsbDevice> devices = new ArrayList<>();
        try {
            UsbManager usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
            Reader reader = new Reader(usbManager);

            for (UsbDevice device : usbManager.getDeviceList().values()) {
                if (reader.isSupported(device)) {
                    devices.add(device);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return devices;
    }

    public static boolean isSupportedDevice(Context context, UsbDevice usbDevice) {
        try {
            UsbManager usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
            Reader reader = new Reader(usbManager);

            return reader.isSupported(usbDevice);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private Context mContext;
    private UsbManager mUsbManager;
    private Reader mReader;
    private PendingIntent mPermissionIntent;

    private int mCurrentSlotNum = 0;
    private String mAtrString = null;

    private UsbDevice mUsbDevice;

    private Listener mListener;

    public RTAcsUsbCommunicator(Context context) {
        super();

        this.mContext = context;

        // Get USB manager
        mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);

        // Initialize reader
        mReader = new Reader(mUsbManager);
        mReader.setOnStateChangeListener(new Reader.OnStateChangeListener() {

            @Override
            public void onStateChange(final int slotNum, int prevState, int currState) {

                String[] stateStrings = { "Unknown", "Absent",
                        "Present", "Swallowed", "Powered", "Negotiable", "Specific" };


                if (prevState < Reader.CARD_UNKNOWN
                        || prevState > Reader.CARD_SPECIFIC) {
                    prevState = Reader.CARD_UNKNOWN;
                }

                if (currState < Reader.CARD_UNKNOWN
                        || currState > Reader.CARD_SPECIFIC) {
                    currState = Reader.CARD_UNKNOWN;
                }

                String outputString = "Slot " + slotNum + ": "
                        + stateStrings[prevState] + " -> "
                        + stateStrings[currState];
                Log.e(TAG, outputString);

                mCurrentSlotNum = slotNum;

                switch (currState) {
                    case Reader.CARD_PRESENT: {

                        // power on card

                        // Get action number
                        final int actionNum = Reader.CARD_WARM_RESET;

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                // Set parameters
                                PowerParams params = new PowerParams();
                                params.slotNum = slotNum;
                                params.action = actionNum;

                                // Perform power action
                                new PowerTask(RTAcsUsbCommunicator.this).execute(params);
                            }
                        });

                        break;
                    }
                    default: {
                        mAtrString = null;
                        break;
                    }
                }

                final int currentState = currState;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mListening && mListener != null) {
                            mListener.onCardStatusChanged(RTAcsUsbCommunicator.this, currentState);
                        }
                    }
                });

            }
        });

    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    public void setContext(Context context) {
        this.mContext = context;
    }

    public void setUsbDevice(UsbDevice usbDevice) {
        if (mUsbDevice != usbDevice) {
            this.mUsbDevice = usbDevice;
        }
    }

    public UsbDevice getUsbDevice() {
        return mUsbDevice;
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    public String getReaderName() {
        if (mReader == null) {
            return null;
        }

        return mReader.getReaderName();
    }

    private String mFirmwareVersion = null;
    public String getFirmwareVersion() {
        return mFirmwareVersion;
    }

    // -- RTRfidCommunicator

    private boolean mListening = false;

    @Override
    public void start() {
        if (mUsbManager == null) {
            postErrorWithoutListening("Usb manager not available.");
            return;
        }

        if (mUsbDevice == null) {
            postErrorWithoutListening("No usb device.");
            return;
        }
        if (!mReader.isSupported(mUsbDevice)) {
            postErrorWithoutListening("This usb device is not supported.");
            return;
        }

        mListening = true;

        // Register receiver for USB permission
        mPermissionIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        mContext.registerReceiver(mReceiver, filter);

        mUsbManager.requestPermission(mUsbDevice, mPermissionIntent);

    }

    @Override
    public void stop() {
        mListening = false;

        mContext.unregisterReceiver(mReceiver);

        // Close reader
        Log.e(TAG, "Closing reader...");
        new CloseTask(this).execute();

    }
    
    /**頁數最小值*/
    private int p2Min = 4;
    
    /**頁數最小值*/
    public void setP2Min(int p2Min) {
        this.p2Min = p2Min;
    }
    
    /**設定 頁數最小值*/
    public int getP2Min() {
        return p2Min;
    }
    
    /**頁數最大值*/
    private int p2Max = 15;
    
    /**設定 頁數最大值*/
    public void setP2Max(int p2Max) {
        this.p2Max = p2Max;
    }
    
    /**頁數最大值*/
    public int getP2Max() {
        return p2Max;
    }
    
    
    /**寫入資料
     * @param p2 存入第N頁 4~15
     * @param s 存入字串 最多 4 byte
     * */
    public void writeData(int p2 , String s) {
        String str = "FF D6 00 ";
        if (p2 < getP2Min() || p2 > getP2Max()){
            p2 = getP2Min();
        }
	    String p2Hex = Integer.toHexString(p2);
	    if (p2Hex.length() < 2){
		    p2Hex = "0" + p2Hex;
	    }
        
        String lenHex = Integer.toHexString(s.length());
        if (lenHex.length() < 2){
            lenHex = "0" + lenHex;
        }
        
        String[] data = new String[s.length()];
        String dataHex = "";
        for (int i = 0; i < s.length(); i++) {
            String hex = Integer.toHexString(s.charAt(i));
            if (hex.length() < 2){
                hex = "0" + hex;
            }
            dataHex += hex;
            if (i < s.length() - 1){
                dataHex += " ";
            }
        }
        str += p2Hex + " " + lenHex + " " + dataHex;
        str = str.replace(" " , "");
        sendApdu(ADBHex.hexString2Bytes(str));
    }

    /**送出指令*/
    @Override
    public void sendApdu(byte[] apdu) {
        // Set parameters
        TransmitParams params = new TransmitParams();
        params.commandType = TRANSMIT_COMMAND_TYPE_GET_CARD_ID;
        params.slotNum = mCurrentSlotNum;
        params.controlCode = -1;
        params.commandString = ADBHex.toHexString(apdu);

        TransmitCallback callback = new TransmitCallback() {
            @Override
            public void onTransmit(TransmitTask task, final String commandString) {
                final RTAcsUsbCommunicator communicator = RTAcsUsbCommunicator.this;
                communicator.mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (communicator.mListening && communicator.mListener != null) {
                            communicator.mListener.onTransmit(communicator, commandString);
                        }
                    }
                });
            }
            
            @Override
            public void onComplete(TransmitTask task, int commandType, final byte[] response, int responseLength) {

                final RTAcsUsbCommunicator communicator = RTAcsUsbCommunicator.this;

                communicator.mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (communicator.mListening && communicator.mListener != null) {
                            communicator.mListener.onResponseToApdu(communicator, response, null);
                        }
                    }
                });

            }

            @Override
            public void onFailed(TransmitTask task, int commandType, Exception e) {
                final String errorMessage = e.toString();

                final RTAcsUsbCommunicator communicator = RTAcsUsbCommunicator.this;

                communicator.mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (communicator.mListening && communicator.mListener != null) {
                            communicator.mListener.onResponseToApdu(communicator, null, errorMessage);
                        }
                    }
                });

            }
        };

        // Transmit APDU
        new TransmitTask(this, callback).execute(params);

    }
    
    /**讀取資料
     * @param p2 讀取第N頁 4~15
     * @param le 讀取byte數
     * */
    public void readData(int p2 , int len) {
        if (p2 < getP2Min() || p2 > getP2Max()){
            p2 = getP2Min();
        }
        String p2Hex = Integer.toHexString(p2);
        if (p2Hex.length() < 2){
            p2Hex = "0" + p2Hex;
        }
    
        String lenHex = Integer.toHexString(len);
        if (lenHex.length() < 2){
            lenHex = "0" + lenHex;
        }
        // Set parameters
        TransmitParams params = new TransmitParams();
        params.commandType = TRANSMIT_COMMAND_TYPE_GET_CARD_ID;
        params.slotNum = mCurrentSlotNum;
        params.controlCode = -1;
        
        params.commandString = "FF B0 00 " + p2Hex + " " + lenHex;
        
        TransmitCallback callback = new TransmitCallback() {
            @Override
            public void onTransmit(TransmitTask task, final String commandString) {
                final RTAcsUsbCommunicator communicator = RTAcsUsbCommunicator.this;
                communicator.mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (communicator.mListening && communicator.mListener != null) {
                            communicator.mListener.onTransmit(communicator, commandString);
                        }
                    }
                });
            }
            
            @Override
            public void onComplete(TransmitTask task, int commandType, final byte[] response, int responseLength) {
                
                final RTAcsUsbCommunicator communicator = RTAcsUsbCommunicator.this;
                
                communicator.mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (communicator.mListening && communicator.mListener != null) {
                            communicator.mListener.onReadData(communicator, response, null);
                        }
                    }
                });
                
            }
            
            @Override
            public void onFailed(TransmitTask task, int commandType, Exception e) {
                final String errorMessage = e.toString();
                
                final RTAcsUsbCommunicator communicator = RTAcsUsbCommunicator.this;
                
                communicator.mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (communicator.mListening && communicator.mListener != null) {
                            communicator.mListener.onReadData(communicator, null, errorMessage);
                        }
                    }
                });
                
            }
        };
        
        // Transmit APDU
        new TransmitTask(this, callback).execute(params);
        
    }

    @Override
    public void powerOffCard() {
        int slotNum = mCurrentSlotNum;
        int actionNum = Reader.CARD_POWER_DOWN;

        // Set parameters
        PowerParams params = new PowerParams();
        params.slotNum = slotNum;
        params.action = actionNum;

        new PowerTask(RTAcsUsbCommunicator.this).execute(params);
    }

    // --



    // --

    private void postError(final String errorMessage) {
        if (!mListening) {
            return;
        }

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.onError(RTAcsUsbCommunicator.this, errorMessage);
                }
            }
        });


    }

    private void postErrorWithoutListening(final String errorMessage) {

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.onError(RTAcsUsbCommunicator.this, errorMessage);
                }
            }
        });

    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            if (ACTION_USB_PERMISSION.equals(action)) {

                synchronized (this) {

                    UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {

                        if (device != null) {
                            Log.e(TAG, "Opening reader: " + device.getDeviceName() + "...");

                            // Open reader
                            new OpenTask(RTAcsUsbCommunicator.this).execute(device);
                        }
                        else {
                            postError("No usb device.");
                        }

                    }
                    else {
                        Log.e(TAG, "Permission denied for device " + device.getDeviceName());

                        postError("Permission denied for device " + device.getDeviceName());

                    }
                }

            }
            else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {

                synchronized (this) {

                    UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (device != null && device.equals(mReader.getDevice())) {
                        Log.e(TAG, "Closing reader...");

                        // Close reader
                        new CloseTask(RTAcsUsbCommunicator.this).execute();

                    }

                }
            }

        }
    };

    private static class OpenTask extends AsyncTask<UsbDevice, Void, Exception> {

        private WeakReference<RTAcsUsbCommunicator> mWeakCommunicator;

        public OpenTask(RTAcsUsbCommunicator communicator) {
            super();

            this.mWeakCommunicator = new WeakReference<>(communicator);

        }
        @Override
        protected Exception doInBackground(UsbDevice... params) {
            Exception result = null;

            try {

                mWeakCommunicator.get().mReader.open(params[0]);

            } catch (Exception e) {

                result = e;
            }

            return result;
        }

        @Override
        protected void onPostExecute(Exception result) {
            final RTAcsUsbCommunicator communicator = mWeakCommunicator.get();

            if (result != null) {
                Log.e(TAG, "open reader failed: "+result.toString());

                communicator.mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        communicator.mListening = false;
                        communicator.mContext.unregisterReceiver(communicator.mReceiver);

                        if (communicator.mListening && communicator.mListener != null) {
                            communicator.mListener.onReaderDisconnected(communicator);
                        }
                    }
                });

            }
            else {
                Log.e(TAG, "open reader: "+communicator.mReader.getReaderName());

                // Get firmware version

                // Set parameters
                TransmitParams params = new TransmitParams();
                params.commandType = TRANSMIT_COMMAND_TYPE_GET_FIRMWARE_VERSION;
                params.slotNum = 0;
                params.controlCode = Reader.IOCTL_CCID_ESCAPE;
                params.commandString = "FF 00 48 00 00";

                TransmitCallback callback = new TransmitCallback() {
                    @Override
                    public void onTransmit(TransmitTask task, final String commandString) {
                        final RTAcsUsbCommunicator communicator = mWeakCommunicator.get();
                        communicator.mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (communicator.mListening && communicator.mListener != null) {
                                    communicator.mListener.onTransmit(communicator, commandString);
                                }
                            }
                        });
                    }
                    
                    @Override
                    public void onComplete(TransmitTask task, int commandType, byte[] response, int responseLength) {

                        final RTAcsUsbCommunicator communicator = mWeakCommunicator.get();

                        try {

                            communicator.mFirmwareVersion = new String(response, "ascii");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        communicator.mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (communicator.mListening && communicator.mListener != null) {
                                    communicator.mListener.onReaderConnected(communicator, communicator.mReader);
                                }
                            }
                        });

                    }

                    @Override
                    public void onFailed(TransmitTask task, int commandType, Exception e) {

                        final RTAcsUsbCommunicator communicator = mWeakCommunicator.get();

                        communicator.mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (communicator.mListening && communicator.mListener != null) {
                                    communicator.mListener.onReaderConnected(communicator, communicator.mReader);
                                }
                            }
                        });

                    }
                };

                // Transmit Escape
                new TransmitTask(mWeakCommunicator.get(), callback).execute(params);

            }
        }
    }

    private static class CloseTask extends AsyncTask<Void, Void, Void> {

        private WeakReference<RTAcsUsbCommunicator> mWeakCommunicator;

        public CloseTask(RTAcsUsbCommunicator communicator) {
            super();
            Log.e(TAG , "CloseTask");
            this.mWeakCommunicator = new WeakReference<>(communicator);

        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.e(TAG , "CloseTask doInBackground");
            try {
                mWeakCommunicator.get().mReader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.e(TAG , "CloseTask onPostExecute");
            Log.e(TAG, "close reader");

            final RTAcsUsbCommunicator communicator = mWeakCommunicator.get();
            communicator.mFirmwareVersion = null;

            communicator.mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (communicator.mListener != null) {
                        communicator.mListener.onReaderDisconnected(communicator);
                    }
                }
            });

        }

    }

    private static class PowerParams {

        public int slotNum;
        public int action;
    }

    private static class PowerResult {

        public int slotNum;
        public int action;

        public byte[] atr;
        public Exception e;
    }

    private static class PowerTask extends AsyncTask<PowerParams, Void, PowerResult> {

        private WeakReference<RTAcsUsbCommunicator> mWeakCommunicator;

        public PowerTask(RTAcsUsbCommunicator communicator) {
            super();
            this.mWeakCommunicator = new WeakReference<>(communicator);

        }

        @Override
        protected PowerResult doInBackground(PowerParams... params) {
            PowerResult result = new PowerResult();
            result.slotNum = params[0].slotNum;
            result.action = params[0].action;

            try {

                result.atr = mWeakCommunicator.get().mReader.power(params[0].slotNum, params[0].action);

            } catch (Exception e) {

                result.e = e;
            }

            return result;
        }

        @Override
        protected void onPostExecute(PowerResult result) {
            if (result.e != null) {
                Log.e(TAG, "Power on card failed: "+result.e.toString());

                // Post error
                mWeakCommunicator.get().postError(result.e.toString());

            }
            else {
                Log.e(TAG, "Power on card: "+ADBHex.toHexString(result.atr));

                if (result.action == Reader.CARD_POWER_DOWN) {
                    mWeakCommunicator.get().mAtrString = null;

                }
                else if (result.action == Reader.CARD_WARM_RESET) {
                    mWeakCommunicator.get().mAtrString = ADBHex.toHexString(result.atr);

                    // Set protocol
                    int preferredProtocols = Reader.PROTOCOL_T0 | Reader.PROTOCOL_T1;

                    // Set Parameters
                    SetProtocolParams params = new SetProtocolParams();
                    params.slotNum = result.slotNum;
                    params.preferredProtocols = preferredProtocols;

                    // Set protocol
                    new SetProtocolTask(mWeakCommunicator.get()).execute(params);

                }

            }
        }
    }

    private static class SetProtocolParams {

        public int slotNum;
        public int preferredProtocols;
    }

    private static class SetProtocolResult {

        public int slotNum;

        public int activeProtocol;
        public Exception e;
    }

    private static class SetProtocolTask extends
            AsyncTask<SetProtocolParams, Void, SetProtocolResult> {

        private WeakReference<RTAcsUsbCommunicator> mWeakCommunicator;

        public SetProtocolTask(RTAcsUsbCommunicator communicator) {
            super();
            this.mWeakCommunicator = new WeakReference<>(communicator);

        }

        @Override
        protected SetProtocolResult doInBackground(SetProtocolParams... params) {
            SetProtocolResult result = new SetProtocolResult();
            result.slotNum = params[0].slotNum;

            try {

                result.activeProtocol = mWeakCommunicator.get().mReader.setProtocol(params[0].slotNum, params[0].preferredProtocols);

            } catch (Exception e) {

                result.e = e;
            }

            return result;
        }

        @Override
        protected void onPostExecute(SetProtocolResult result) {
            if (result.e != null) {
                Log.e(TAG, "set protocol failed: "+result.e.toString());

                // Post error
                mWeakCommunicator.get().postError(result.e.toString());

            }
            else {
                String activeProtocolString = "Active Protocol: ";

                switch (result.activeProtocol) {

                    case Reader.PROTOCOL_T0:
                        activeProtocolString += "T=0";
                        break;

                    case Reader.PROTOCOL_T1:
                        activeProtocolString += "T=1";
                        break;

                    default:
                        activeProtocolString += "Unknown";
                        break;
                }

                Log.e(TAG, "set protocol: "+activeProtocolString);

                // get card number

                // Set parameters
                TransmitParams params = new TransmitParams();
                params.commandType = TRANSMIT_COMMAND_TYPE_GET_CARD_ID;
                params.slotNum = result.slotNum;
                params.controlCode = -1;
                params.commandString = "FF CA 00 00 00";

                TransmitCallback callback = new TransmitCallback() {
                    @Override
                    public void onTransmit(TransmitTask task, final String commandString) {
                        final RTAcsUsbCommunicator communicator = mWeakCommunicator.get();
                        communicator.mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (communicator.mListening && communicator.mListener != null) {
                                    communicator.mListener.onTransmit(communicator, commandString);
                                }
                            }
                        });
                    }
    
                    @Override
                    public void onComplete(TransmitTask task, int commandType, byte[] response, int responseLength) {

                        String errorMessage = null;

                        String tagID = ADBHex.toHexString(response);
                        tagID = tagID.replace(" ", "");

                        byte checkByte1 = response[response.length-2];
                        byte checkByte2 = response[response.length-1];
                        if (checkByte1 == (byte) 0x90 && checkByte2 == 0x00) {
                            tagID = tagID.toUpperCase(Locale.US);
                            tagID = tagID.substring(0, tagID.length()-4);
                        }
                        else if (checkByte1 == (byte) 0x62 && checkByte2 == (byte) 0x82) {
                            errorMessage = "End of UID/ATS reached before Le bytes (Le is greater than UID Length)";
                        }
                        else if (checkByte1 == (byte) 0x63 && checkByte2 == 0x00) {
                            errorMessage = "The operation is failed";
                        }
                        else if (checkByte1 == (byte) 0x6A && checkByte2 == (byte) 0x81) {
                            errorMessage = "Function not supported";
                        }
                        else if (checkByte1 == (byte) 0x6C) {
                            errorMessage = "Wrong length (wrong number Le: ‘"+checkByte2+"’ encodes the exact number) if Le is less than the available UID length";
                        }
                        else {
                            errorMessage = "Other failure";
                        }

                        final RTAcsUsbCommunicator communicator = mWeakCommunicator.get();

                        final String finalTagID = tagID;
                        final String finalErrMsg = errorMessage;
                        communicator.mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (communicator.mListening && communicator.mListener != null) {
                                    communicator.mListener.onFindTag(communicator, finalTagID, communicator.mAtrString, finalErrMsg);
                                }
                            }
                        });

                    }

                    @Override
                    public void onFailed(TransmitTask task, int commandType, Exception e) {
                        final String errorMessage = e.toString();

                        final RTAcsUsbCommunicator communicator = mWeakCommunicator.get();

                        communicator.mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (communicator.mListening && communicator.mListener != null) {
                                    communicator.mListener.onFindTag(communicator, null, communicator.mAtrString, errorMessage);
                                }
                            }
                        });

                    }
                };

                // Transmit APDU
                new TransmitTask(mWeakCommunicator.get(), callback).execute(params);

            }
        }
    }

    private static final int TRANSMIT_COMMAND_TYPE_GET_FIRMWARE_VERSION = -50;
    private static final int TRANSMIT_COMMAND_TYPE_GET_CARD_ID = -100;

    private static class TransmitParams {

        public int commandType;

        public int slotNum;
        public int controlCode;
        public String commandString;
    }

    private static class TransmitProgress {

        public int commandType;

        public int slotNum;

        public int controlCode;
        public byte[] command;
        public int commandLength;
        public byte[] response;
        public int responseLength;
        public Exception e;
    }

    private interface TransmitCallback {
        void onTransmit(TransmitTask task, String commandString);
        void onComplete(TransmitTask task, int commandType, byte[] response, int responseLength);
        void onFailed(TransmitTask task, int commandType, Exception e);
    }

    private static class TransmitTask extends
            AsyncTask<TransmitParams, TransmitProgress, Void> {

        private WeakReference<RTAcsUsbCommunicator> mWeakCommunicator;
        private TransmitCallback mCallback;

        public TransmitTask(RTAcsUsbCommunicator communicator, TransmitCallback callback) {
            super();

            this.mWeakCommunicator = new WeakReference<>(communicator);
            this.mCallback = callback;

        }

        @Override
        protected Void doInBackground(TransmitParams... params) {
            TransmitProgress progress;

            byte[] command;
            byte[] response;
            int responseLength;
            int foundIndex;
            int startIndex = 0;

            do {

                // Find carriage return
                foundIndex = params[0].commandString.indexOf('\n', startIndex);
                if (foundIndex >= 0) {
                    command = ADBHex.hexString2Bytes(params[0].commandString.substring(
                            startIndex, foundIndex).replace(" ", ""));
                }
                else {
                    command = ADBHex.hexString2Bytes(params[0].commandString
                            .substring(startIndex).replace(" ", ""));
                }

                // Set next start index
                startIndex = foundIndex + 1;

                response = new byte[300];
                progress = new TransmitProgress();
                progress.commandType = params[0].commandType;
                progress.slotNum = params[0].slotNum;
                progress.controlCode = params[0].controlCode;
                try {
                    if (mCallback != null) {
                        mCallback.onTransmit(this, params[0].commandString);
                    }
                    if (params[0].controlCode < 0) {

                        // Transmit APDU
                        responseLength = mWeakCommunicator.get().mReader.transmit(params[0].slotNum,
                                                                                    command,
                                                                                    command.length,
                                                                                    response,
                                                                                    response.length);

                    }else {

                        // Transmit control command
                        responseLength = mWeakCommunicator.get().mReader.control(params[0].slotNum,
                                                                                    params[0].controlCode,
                                                                                    command,
                                                                                    command.length,
                                                                                    response,
                                                                                    response.length);
                    }

                    progress.command = command;
                    progress.commandLength = command.length;
                    progress.response = Arrays.copyOf(response, responseLength);
                    progress.responseLength = responseLength;
                    progress.e = null;

                } catch (Exception e) {

                    progress.command = null;
                    progress.commandLength = 0;
                    progress.response = null;
                    progress.responseLength = 0;
                    progress.e = e;
                }

                publishProgress(progress);

            } while (foundIndex >= 0);

            return null;
        }

        @Override
        protected void onProgressUpdate(TransmitProgress... progress) {
            if (progress[0].e != null) {
                Log.e(TAG, "transmit failed: "+progress[0].e.toString());

                if (mCallback != null) {
                    mCallback.onFailed(this, progress[0].commandType, progress[0].e);
                }

            }
            else {
                Log.e(TAG, "transmit success: "+ADBHex.toHexString(progress[0].command)+", response: "+ADBHex.toHexString(progress[0].response));

                if (mCallback != null) {
                    mCallback.onComplete(this, progress[0].commandType, progress[0].response, progress[0].responseLength);
                }

            }
        }
    }

}
