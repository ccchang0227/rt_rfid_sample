package com.realtouchapp.rt_rfid_sample;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.acs.smartcard.Reader;
import com.realtouchapp.rt_rfid_library.ADBHex;
import com.realtouchapp.rt_rfid_library.RTAcsUsbCommunicator;
import com.realtouchapp.rt_rfid_library.RTRfidCommunicator;

import java.util.List;

public class UsbReaderActivity extends Activity {
    private final static String TAG = "UsbReaderActivity";

    private RTAcsUsbCommunicator communicator;

    

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usb_reader);

        setView();

        communicator = new RTAcsUsbCommunicator(this.getApplicationContext());

        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        registerReceiver(receiver, filter);

    }
    
    private ScrollView sv;
    private TextView tv;
    private EditText et_write;
    private Button btn_read;
    private Button btn_write;
    private void setView(){
        sv = findViewById(R.id.sv);
    
        tv = findViewById(R.id.tv);
        tv.append("Starting...\n");
        tv.append("----------------------\n");
        //
        et_write = findViewById(R.id.et_write);
        btn_read = findViewById(R.id.btn_read);
        btn_write = findViewById(R.id.btn_write);
        
        btn_read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                communicator.readData(4,4);
            }
        });
        
        btn_write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = et_write.getText().toString().trim();
                if (str.length() > 0){
                    communicator.writeData(4 , str);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        List<UsbDevice> devices = RTAcsUsbCommunicator.getSupportedDevices(this.getApplicationContext());
        if (devices.size() > 0) {
            communicator.setUsbDevice(devices.get(0));
            communicator.setListener(listener);
            communicator.start();
        }

    }

    @Override
    protected void onStop() {

        communicator.stop();
        communicator.setListener(null);

        super.onStop();
    }

    @Override
    protected void onDestroy() {

        unregisterReceiver(receiver);

        super.onDestroy();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(intent.getAction())) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null && RTAcsUsbCommunicator.isSupportedDevice(context.getApplicationContext(), device)) {

                    communicator.setUsbDevice(device);
                    communicator.setListener(listener);
                    communicator.start();
                    
                }
            }

        }
    };

    private void autoScrollToLastLine() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sv.fullScroll(View.FOCUS_DOWN);
            }
        }, 100);
    }

    private RTAcsUsbCommunicator.Listener listener = new RTAcsUsbCommunicator.Listener() {
        @Override
        public void onTransmit(RTAcsUsbCommunicator communicator, String commandString) {
            Log.e(TAG, "onTransmit: "+commandString);
    
            tv.append("onTransmit: "+commandString+"\n");
    
            autoScrollToLastLine();
        }
    
        @Override
        public void onError(RTRfidCommunicator communicator, String errorMessage) {
            Log.e(TAG, "onError: "+errorMessage);

            tv.append("onError: "+errorMessage+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onReaderConnected(RTAcsUsbCommunicator communicator, Reader reader) {
            Log.e(TAG, "onReaderConnected: "+communicator.getReaderName());

            tv.append("onReaderConnected: "+communicator.getReaderName()+"\n");
            tv.append("  firmware version: "+communicator.getFirmwareVersion()+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onReaderDisconnected(final RTRfidCommunicator communicator) {
            Log.e(TAG, "onReaderDisconnected: ");

            tv.append("onReaderDisconnected: "+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onFindTag(RTRfidCommunicator communicator, String tagID, String atrString, String errorMessage) {
            Log.e(TAG, "onFindTag: "+tagID+", ATR: "+atrString+", error:"+errorMessage);

            tv.append("onFindTag: "+tagID+"\n");
            tv.append("  atrString: "+atrString+"\n");
            tv.append("  errorMessage: "+errorMessage+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onCardStatusChanged(RTAcsUsbCommunicator communicator, int cardStatus) {
            Log.e(TAG, "onCardStatusChanged: "+getCardStatusString(cardStatus));

            tv.append("onCardStatusChanged: "+getCardStatusString(cardStatus)+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

//        @Override
//        public void onResponseToEscapeCommand(RTAcsBtCommunicator communicator, byte[] response, String errorMessage) {
//            Log.e(TAG, "onResponseToEscapeCommand: "+ADBHex.toHexString(response)+", error: "+errorMessage);
//
//            tv.append("onResponseToEscapeCommand: "+ADBHex.toHexString(response)+"\n");
//            tv.append("  errorMessage: "+errorMessage+"\n");
//            tv.append("-------------------------------\n");
//
//            autoScrollToLastLine();
//
//        }

        @Override
        public void onResponseToApdu(RTAcsUsbCommunicator communicator, byte[] response, String errorMessage) {
            Log.e(TAG, "onResponseToApdu: "+ ADBHex.toHexString(response)+", error: "+errorMessage);

            tv.append("onResponseToApdu: "+ADBHex.toHexString(response)+"\n");
            tv.append("  errorMessage: "+errorMessage+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }
    
        @Override
        public void onReadData(RTAcsUsbCommunicator communicator, byte[] response, String errorMessage) {
            Log.e(TAG, "onReadData: "+ ADBHex.toHexString(response)+", error: "+errorMessage);
            
            tv.append("onReadData: "+ADBHex.toHexString(response)+"\n");
    
            String responseString = ADBHex.toHexString(response);
            String[] arr = responseString.split("\\ ");
            String str = "";
            for (int i = 0; i < arr.length - 2; i++) {
                String s = arr[i];
                str +=  (char)(Integer.parseInt(s , 16) & 0xFF);
//                str +=  Integer.parseInt(s , 16) + ",";

//                int i = Integer.parseInt(s , 16) & 0xFF;
//                str +=  Character.toString((char)i) + ",";
            }
            tv.append("onReadData ascii: "+str+"\n");
            Log.e(TAG, "onReadData ascii: "+ str);
            
            tv.append("  errorMessage: "+errorMessage+"\n");
            tv.append("-------------------------------\n");
    
            autoScrollToLastLine();
        }
    };

    private static final String[] stateStrings = { "Unknown", "Absent",
            "Present", "Swallowed", "Powered", "Negotiable", "Specific" };

    /* Get the Card status string. */
    private String getCardStatusString(int cardStatus) {
        if (cardStatus >= 0 && cardStatus < stateStrings.length) {
            return stateStrings[cardStatus];
        }

        return "The card status is unknown.";
    }

}
