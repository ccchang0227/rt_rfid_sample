package com.realtouchapp.rt_rfid_sample;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.acs.bluetooth.BluetoothReader;
import com.realtouchapp.rt_rfid_library.ADBHex;
import com.realtouchapp.rt_rfid_library.RTAcsBtCommunicator;
import com.realtouchapp.rt_rfid_library.RTRfidCommunicator;

public class BtReaderActivity extends Activity {
    private final static String TAG = "BtReaderActivity";

    private RTAcsBtCommunicator communicator;

    private ScrollView sv;
    private TextView tv;

    private Handler handler = new Handler();

    private boolean coarse_location_granted = false;
    private final static int MY_PERMISSIONS_REQUEST_ACCESS_COARSE = 12345;

    // 當使用者點選「同意」或「不同意」之後，會呼叫這個callback函式，接著就可以根據同意或不同意來判斷是否開始使用RFID Communicator
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_COARSE) {
            // Called after user taps "Allow" or "Deny" for location permission.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.w(TAG, "ACCESS_COARSE_LOCATION granted");
                coarse_location_granted = true;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Note this must call on uiThread.
                        communicator.setListener(listener);
                        communicator.start();
                    }
                });

            }
            else {
                coarse_location_granted = false;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(BtReaderActivity.this, "Permission Denied\nThis app needs location access", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bt_reader);

        sv = findViewById(R.id.sv);

        tv = findViewById(R.id.tv);
        tv.append("Starting...\n");
        tv.append("----------------------\n");

        // 針對android 6.0以上的機器，在使用藍牙之前必須先請求location的權限，因此在取得權限之前，並不會呼叫getCardNumber()
        // 要使用權限判斷的方法，必須import android-support-v4的library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // >= android 6.0
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    coarse_location_granted = true;
                }
                else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_COARSE);
                }
            }
            else {
                coarse_location_granted = true;
            }
        }
        else {
            coarse_location_granted = true;
        }

        communicator = new RTAcsBtCommunicator.Builder(getApplicationContext())
                                                .setSpecificDeviceId("rr431-011052")//005230
                                                .setTransmitPower(RTAcsBtCommunicator.TransmitPower.High)
                                                .setSleepMode(RTAcsBtCommunicator.SleepMode.None)
                                                .create();


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (coarse_location_granted) {
            communicator.setListener(listener);
            communicator.start();
        }

    }

    @Override
    protected void onPause() {

        communicator.stop();
        communicator.setListener(null);

        super.onPause();
    }

    private void autoScrollToLastLine() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sv.fullScroll(View.FOCUS_DOWN);
            }
        }, 100);
    }

    private RTAcsBtCommunicator.Listener listener = new RTAcsBtCommunicator.Listener() {
        @Override
        public void onError(RTRfidCommunicator communicator, String errorMessage) {
            Log.e(TAG, "onError: "+errorMessage);

            tv.append("onError: "+errorMessage+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onReaderConnected(RTAcsBtCommunicator communicator, BluetoothReader reader) {
            Log.e(TAG, "onReaderConnected: "+communicator.getReaderName());

            tv.append("onReaderConnected: "+communicator.getReaderName()+"\n");
            tv.append("  serial number: "+communicator.getSerialNumber()+"\n");
            tv.append("  model number: "+communicator.getModelNumber()+"\n");
            tv.append("  system ID: "+communicator.getSystemID()+"\n");
            tv.append("  firmware version: "+communicator.getFirmwareVersion()+"\n");
            tv.append("  hardware version: "+communicator.getHardwareVersion()+"\n");
            tv.append("  manufacturer: "+communicator.getManufacturerName()+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onReaderDisconnected(final RTRfidCommunicator communicator) {
            Log.e(TAG, "onReaderDisconnected: ");

            tv.append("onReaderDisconnected: "+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    communicator.start();
                }
            }, 3000);

        }

        @Override
        public void onFindTag(RTRfidCommunicator communicator, String tagID, String atrString, String errorMessage) {
            Log.e(TAG, "onFindTag: "+tagID+", ATR: "+atrString+", error:"+errorMessage);

            tv.append("onFindTag: "+tagID+"\n");
            tv.append("  atrString: "+atrString+"\n");
            tv.append("  errorMessage: "+errorMessage+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onCardStatusChanged(RTAcsBtCommunicator communicator, int cardStatus) {
            Log.e(TAG, "onCardStatusChanged: "+getCardStatusString(cardStatus));

            tv.append("onCardStatusChanged: "+getCardStatusString(cardStatus)+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onResponseToEscapeCommand(RTAcsBtCommunicator communicator, byte[] response, String errorMessage) {
            Log.e(TAG, "onResponseToEscapeCommand: "+ADBHex.toHexString(response)+", error: "+errorMessage);

            tv.append("onResponseToEscapeCommand: "+ADBHex.toHexString(response)+"\n");
            tv.append("  errorMessage: "+errorMessage+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onResponseToApdu(RTAcsBtCommunicator communicator, byte[] response, String errorMessage) {
            Log.e(TAG, "onResponseToApdu: "+ADBHex.toHexString(response)+", error: "+errorMessage);

            tv.append("onResponseToApdu: "+ADBHex.toHexString(response)+"\n");
            tv.append("  errorMessage: "+errorMessage+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onBatteryStatusChanged(RTAcsBtCommunicator communicator, int status) {
            Log.e(TAG, "onBatteryStatusChanged: "+getBatteryStatusString(status));

            tv.append("onBatteryStatusChanged: "+getBatteryStatusString(status)+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }

        @Override
        public void onBatteryLevelChanged(RTAcsBtCommunicator communicator, int level) {
            Log.e(TAG, "onBatteryLevelChanged: "+getBatteryLevelString(level));

            tv.append("onBatteryLevelChanged: "+getBatteryLevelString(level)+"\n");
            tv.append("-------------------------------\n");

            autoScrollToLastLine();

        }
    };

    /* Get the Card status string. */
    private String getCardStatusString(int cardStatus) {
        if (cardStatus == BluetoothReader.CARD_STATUS_ABSENT) {
            return "Absent.";
        }
        else if (cardStatus == BluetoothReader.CARD_STATUS_PRESENT) {
            return "Present.";
        }
        else if (cardStatus == BluetoothReader.CARD_STATUS_POWERED) {
            return "Powered.";
        }
        else if (cardStatus == BluetoothReader.CARD_STATUS_POWER_SAVING_MODE) {
            return "Power saving mode.";
        }

        return "The card status is unknown.";
    }

    /* Get the Battery level string. */
    private String getBatteryLevelString(int batteryLevel) {
        if (batteryLevel < 0 || batteryLevel > 100) {
            return "Unknown.";
        }

        return String.valueOf(batteryLevel) + "%";
    }

    /* Get the Battery status string. */
    private String getBatteryStatusString(int batteryStatus) {
        if (batteryStatus == BluetoothReader.BATTERY_STATUS_NONE) {
            return "No battery.";
        }
        else if (batteryStatus == BluetoothReader.BATTERY_STATUS_FULL) {
            return "The battery is full.";
        }
        else if (batteryStatus == BluetoothReader.BATTERY_STATUS_USB_PLUGGED) {
            return "The USB is plugged.";
        }

        return "The battery is low.";
    }

}
